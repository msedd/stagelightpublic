
/*
  ARTNET RECEIVER V2

  This SCRIPT allows you to use arduino with ethernet shield or wifi shield and recieve artnet data. Up to you to use channels as you want.


  If you have implemented ameliorations to this sketch, please, contribute by sending back modifications, ameliorations, derivative sketch. It will be a pleasure
  to let them accessible to community



  For VVVV patchers, please think to put a mainloop node in your patch: minimum frame rate is 40fps with bulbs. But do not go too much higher ( max 100 fps).

  This sketch is part of white cat lighting board suite: /http://www.le-chat-noir-numerique.fr
  wich is sending data to many different types of devices, and includes a direct communication in serial also with arduino likes devices
  you may find whitecat interresting because its theatre based logic ( cuelist and automations) AND live oriented ( masters, midi, etc)

  (c)Christoph Guillermet
  http://www.le-chat-noir-numerique.fr
  karistouf@yahoo.fr
*/

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>          // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#define short_get_high_byte(x) ((HIGH_BYTE & x) >> 8)
#define short_get_low_byte(x)  (LOW_BYTE & x)
#define bytes_to_short(h,l) ( ((h << 8) & 0xff00) | (l & 0x00FF) );
// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS     40
#define PIXELINLINE   5
#define LINES         8

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

//MAC and IP of the ethernet shield
//MAC adress of the ethershield is stamped down the shield
//to translate it from hexa decimal to decimal, use: http://www.frankdevelopper.com/outils/convertisseur.php
//HARDWARE
byte mac[] = {  144, 162, 218, 00, 16, 96  };//the mac adress of ethernet shield or uno shield board
IPAddress  ip(192, 168, 0, 120 ); // the IP adress of your device, that should be in same universe of the network you are using, here: 192.168.1.x

// the next two variables are set when a packet is received
byte remoteIp[4];        // holds received packet's originating IP
unsigned int remotePort; // holds received packet's originating port

//customisation: edit this if you want for example read and copy only 4 or 6 channels from channel 12 or 48 or whatever.

const int number_of_channels = 32; //512 for 512 channels
const int start_address = 0; // 0 if you want to read from channel 1

//edit this with SubnetID + UniverseID you want to receive
byte SubnetID = {
  0
};
byte UniverseID = {
  0
};
short select_universe = ((SubnetID * 16) + UniverseID);


//buffers
const int MAX_BUFFER_UDP = 768;
char packetBuffer[MAX_BUFFER_UDP]; //buffer to store incoming data
byte buffer_channel_arduino[number_of_channels]; //buffer to store filetered DMX data

// art net parameters
unsigned int localPort = 6454;      // artnet UDP port is by default 6454
const int art_net_header_size = 17;
const int max_packet_size = 576;
char ArtNetHead[8] = "Art-Net";
char OpHbyteReceive = 0;
char OpLbyteReceive = 0;
short is_artnet_version_1 = 0;
short is_artnet_version_2 = 0;
short seq_artnet = 0;
short artnet_physical = 0;
short incoming_universe = 0;
boolean is_opcode_is_dmx = 0;
boolean is_opcode_is_artpoll = 0;
boolean match_artnet = 1;
short Opcode = 0;
EthernetUDP Udp;

void setup() {
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  pixels.begin(); // This initializes the NeoPixel library.
  Ethernet.begin(mac);//, ip);
  Serial.begin(115200);
  Udp.begin(localPort);
  drawLine(3, 100, 0, 0);
  drawLine(5, 0, 100, 0);
  drawLine(7, 0, 0, 100);
  Serial.println("setup finished");
}

void loop() {

  int packetSize = Udp.parsePacket();
  Serial.println("loop");

  //FIXME: test/debug check
  if (packetSize > art_net_header_size && packetSize <= max_packet_size) { //check size to avoid unneeded checks
    //if(packetSize) {

    IPAddress remote = Udp.remoteIP();
    remotePort = Udp.remotePort();
    Udp.read(packetBuffer, MAX_BUFFER_UDP);

    //read header
    match_artnet = 1;
    for (int i = 0; i < 7; i++) {
      //if not corresponding, this is not an artnet packet, so we stop reading
      if (char(packetBuffer[i]) != ArtNetHead[i]) {
        match_artnet = 0;
        break;
      }
    }

    //if its an artnet header
    if (match_artnet == 1) {
      //artnet protocole revision, not really needed
      //is_artnet_version_1=packetBuffer[10];
      //is_artnet_version_2=packetBuffer[11];*/

      //sequence of data, to avoid lost packets on routeurs
      //seq_artnet=packetBuffer[12];*/

      //physical port of  dmx N°
      //artnet_physical=packetBuffer[13];*/

      //operator code enables to know wich type of message Art-Net it is
      Opcode = bytes_to_short(packetBuffer[9], packetBuffer[8]);

      //if opcode is DMX type
      if (Opcode == 0x5000) {
        is_opcode_is_dmx = 1;
        is_opcode_is_artpoll = 0;
      }

      //if opcode is artpoll
      else if (Opcode == 0x2000) {
        is_opcode_is_artpoll = 1;
        is_opcode_is_dmx = 0;
        //( we should normally reply to it, giving ip adress of the device)
      }

      //if its DMX data we will read it now
      if (is_opcode_is_dmx = 1) {

        //read incoming universe
        incoming_universe = bytes_to_short(packetBuffer[15], packetBuffer[14])
                            //if it is selected universe DMX will be read
        if (incoming_universe == select_universe) {

          //getting data from a channel position, on a precise amount of channels, this to avoid to much operation if you need only 4 channels for example
          //channel position
          Serial.print("empfange daten");
          for (int i = start_address; i < number_of_channels; i++) {
            buffer_channel_arduino[i - start_address] = byte(packetBuffer[i + art_net_header_size + 1]);
          }
        }
      }
    }//end of sniffing
    
    for (int i = 0; i < LINES; i++) {

      int r = buffer_channel_arduino[(i * 3) + 0];
      int g = buffer_channel_arduino[(i * 3) + 1];
      int b = buffer_channel_arduino[(i * 3) + 2];
      
      drawLine(i, r, g, b);

    }
  }
}

void drawLine(int line, int red, int green, int blue) {
  for (int i = 0; i < PIXELINLINE; i++) {
    int index = i * LINES + line;
    pixels.setPixelColor(index, pixels.Color(red, green, blue));
    //Serial.print("--> PIXEL ");
    //Serial.println(index);
  }
  pixels.show(); // This sends the updated pixel color to the hardware.
}
